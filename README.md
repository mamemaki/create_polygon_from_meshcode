# get_polygon_from_meshcode

## 説明

メッシュコードからポリゴンを、ポイントからメッシュコードを生成するためのplpgsql関数です。

JIS X 0410に準拠した地域メッシュコードと浸水想定区域図データ電子化ガイドラインに準拠したメッシュコードに対応しています。

* https://kikakurui.com/x0/X0410-2002-01.html
* https://www.stat.go.jp/data/mesh/pdf/gaiyo1.pdf
* https://www.mlit.go.jp/common/001097667.pdf

## インストール

```shell
psql -U postgres -d <database> -f get_polygon_from_meshcode.sql
```

## 使い方
### メッシュコードからポリゴンを生成
`get_polygon_from_meshcode`でメッシュコードからポリゴンを生成できます。

```sql
db=# SELECT st_astext(get_polygon_from_meshcode('533946'));
                                                      st_astext
----------------------------------------------------------------------------------------------------------------------
 POLYGON((139.75 35.666666666666664,139.75 35.75,139.875 35.75,139.875 35.666666666666664,139.75 35.666666666666664))
(1 row)
                                                      
db=# SELECT st_astext(get_polygon_from_meshcode('53394621'));
                                                                    st_astext
-------------------------------------------------------------------------------------------------------------------------------------------------
 POLYGON((139.7625 35.68333333333333,139.7625 35.69166666666667,139.775 35.69166666666667,139.775 35.68333333333333,139.7625 35.68333333333333))
(1 row)

db=# SELECT st_astext(get_polygon_from_meshcode('5339462143'));
                                                                        st_astext
----------------------------------------------------------------------------------------------------------------------------------------------------------
 POLYGON((139.76875 35.68958333333333,139.76875 35.69166666666667,139.771875 35.69166666666667,139.771875 35.68958333333333,139.76875 35.68958333333333))
(1 row)

db=# SELECT st_astext(get_polygon_from_meshcode('5339462110906'));
                                                                   st_astext
-----------------------------------------------------------------------------------------------------------------------------------------------
 POLYGON((139.77 35.69083333333333,139.77 35.69166666666667,139.77125 35.69166666666667,139.77125 35.69083333333333,139.77 35.69083333333333))
(1 row)

db=# SELECT st_astext(get_polygon_from_meshcode('5339462179560'));
                                                      st_astext
----------------------------------------------------------------------------------------------------------------------
 POLYGON((139.77 35.69125,139.77 35.69133333333333,139.770125 35.69133333333333,139.770125 35.69125,139.77 35.69125))
(1 row)

db=# SELECT st_astext(get_polygon_from_meshcode('533946217950602'));
                                                             st_astext
------------------------------------------------------------------------------------------------------------------------------------
 POLYGON((139.770025 35.69125,139.770025 35.69125833333333,139.7700375 35.69125833333333,139.7700375 35.69125,139.770025 35.69125))
(1 row)
```

#### 3次メッシュの1/10細分区間と1/20細分区間

3次メッシュの1/10細分区間と1/20細分区間はメッシュコードの仕様がイマイチなため自動検出できません。そのため、メッシュコードの種別を指定する必要があります。

```sql
db=# -- 1/4メッシュ扱いで出力されてしまう
db=# SELECT st_astext(get_polygon_from_meshcode('5339462196'));
                                                                       st_astext
-------------------------------------------------------------------------------------------------------------------------------------------------------
 POLYGON((139.7625 35.68333333333333,139.7625 35.68541666666667,139.765625 35.68541666666667,139.765625 35.68333333333333,139.7625 35.68333333333333))
(1 row)
db=# -- 1/10メッシュとして出力するにはmesh_typeを指定する
db=# SELECT st_astext(get_polygon_from_meshcode('5339462196', mesh_type:='1/10_mesh_10char'));
                                                                   st_astext
-----------------------------------------------------------------------------------------------------------------------------------------------
 POLYGON((139.77 35.69083333333333,139.77 35.69166666666667,139.77125 35.69166666666667,139.77125 35.69083333333333,139.77 35.69083333333333))
(1 row)
```

### 緯度経度からメッシュコードを取得
`get_meshcode_from_point`で緯度経度からメッシュコードを取得できます。

```sql
db=# SELECT get_meshcode_from_point(139.7700274114246, 35.691254676389114, '1/4_mesh');
 get_meshcode_from_point
-------------------------
 5339462143
(1 row)

db=# SELECT get_meshcode_from_point(139.7700274114246, 35.691254676389114, '1/10_mesh');
 get_meshcode_from_point
-------------------------
 5339462110906
(1 row)

db=# SELECT get_meshcode_from_point(139.7700274114246, 35.691254676389114, '1/100_mesh');
 get_meshcode_from_point
-------------------------
 5339462179560
(1 row)

db=# SELECT get_meshcode_from_point(139.7700274114246, 35.691254676389114, '1/1000_mesh');
 get_meshcode_from_point
-------------------------
 533946217950602
(1 row)
```

### メッシュ種別を確認する
`get_mesh_type`関数でメッシュコードからメッシュ種別を確認できます。
```sql
db=# SELECT get_mesh_type('5339');
 get_mesh_type
---------------
 1st_mesh
(1 row)
 
db=# SELECT get_mesh_type('5339462179560');
 get_mesh_type
---------------
 1/100_mesh
(1 row)
```

### ジオメトリと交わるメッシュを取得する
`get_mesh_polygons_on_geometry`関数でジオメトリと交わるメッシュを取得できます。

#### 例: 千代田区の3次メッシュと1/10細分区間を取得
```sql
CREATE TABLE chiyoda_3rd_meshes AS
WITH chiyodaku AS (SELECT geom
                   FROM tokyo_boundary
                   WHERE n03_004 = '千代田区'),
     meshes AS (SELECT get_mesh_polygons_on_geometry(geom, '3rd_mesh') AS mesh
                FROM chiyodaku)
SELECT (mesh).mesh_code, (mesh).geom
FROM meshes;

CREATE TABLE chiyoda_1_10th_meshes AS
WITH chiyodaku AS (SELECT geom
                   FROM tokyo_boundary
                   WHERE n03_004 = '千代田区'),
     meshes AS (SELECT get_mesh_polygons_on_geometry(geom, '1/10_mesh_10char') AS mesh
                FROM chiyodaku)
SELECT (mesh).mesh_code, (mesh).geom
FROM meshes;
```

![chiyoda_mesh_sample](docs/chiyoda_mesh_sample.png)

#### 例: 河川と交わるメッシュを取得
```sql
CREATE TABLE ootagawa_2nd_mesh AS
WITH mesh_all AS (SELECT gid, get_mesh_polygons_on_geometry(geom, '2nd_mesh') AS mesh
                  FROM ootagawa
                  WHERE w05_004 = '太田川')
SELECT gid AS rid, (m.mesh).mesh_code, (m.mesh).mesh_type, (m.mesh).geom
FROM mesh_all AS m;

CREATE TABLE ootagawa_3rd_mesh AS
WITH mesh_all AS (SELECT gid, get_mesh_polygons_on_geometry(geom, '3rd_mesh') AS mesh
                  FROM ootagawa
                  WHERE w05_004 = '太田川')
SELECT gid AS rid, (m.mesh).mesh_code, (m.mesh).mesh_type, (m.mesh).geom
FROM mesh_all AS m;

CREATE TABLE ootagawa_1_10th_mesh AS
WITH mesh_all AS (SELECT gid, get_mesh_polygons_on_geometry(geom, '1/10_mesh_10char') AS mesh
                  FROM ootagawa
                  WHERE w05_004 = '太田川')
SELECT gid AS rid, (m.mesh).mesh_code, (m.mesh).mesh_type, (m.mesh).geom
FROM mesh_all AS m;
```

![ootagawa_mesh_sample](docs/ootagawa_mesh_sample.png)


## サポートしているメッシュコードの種類
`supported_mesh_types`関数でサポートしているメッシュコードの種類を確認できます。
```sql
SELECT * FROM supported_mesh_types();
```

2倍地域メッシュコード、5倍地域メッシュコードはもういらないと思うので作っていません。

| mesh\_type | description |
| :--- | :--- |
| 1st\_mesh | 1次メッシュ:4桁 |
| 2nd\_mesh | 2次メッシュ:6桁 |
| 3rd\_mesh | 3次メッシュ:8桁 |
| 1/2\_mesh | 1/2メッシュ/4次メッシュ\(500m\):9桁 |
| 1/4\_mesh | 1/4メッシュ/5次メッシュ\(250m\):10桁 |
| 1/8\_mesh | 1/8メッシュ/6次メッシュ\(125m\):11桁 |
| 1/10\_mesh\_10char | 3次メッシュの1/10細分区間\(100m\):10桁 自動検出不可 |
| 1/20\_mesh\_11char | 3次メッシュの1/20細分区間\(50m\):11桁 自動検出不可 |
| 1/40\_mesh\_12char | 3次メッシュの1/40細分区間\(25m\):12桁 |
| 1/10\_mesh | 1/10メッシュ\(100m\):13桁 |
| 1/20\_mesh | 1/20メッシュ\(50m\):13桁 |
| 1/25\_mesh | 1/25メッシュ\(40m\):13桁 |
| 1/40\_mesh | 1/40メッシュ\(25m\):13桁 |
| 1/50\_mesh | 1/50メッシュ\(20m\):13桁 |
| 1/80\_mesh | 1/80メッシュ\(12.5m\):13桁 |
| 1/100\_mesh | 1/100メッシュ\(10m\):13桁 |
| 1/200\_mesh | 1/200メッシュ\(5m\):15桁 |
| 1/250\_mesh | 1/250メッシュ\(4m\):15桁 |
| 1/400\_mesh | 1/400メッシュ\(2.5m\):15桁 |
| 1/500\_mesh | 1/500メッシュ\(2m\):15桁 |
| 1/800\_mesh | 1/800メッシュ\(1.25m\):15桁 |
| 1/1000\_mesh | 1/1000メッシュ\(1m\):15桁 |

