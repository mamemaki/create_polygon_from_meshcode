CREATE EXTENSION IF NOT EXISTS postgis;

DROP TYPE IF EXISTS MeshType CASCADE;
-- メッシュタイプ
CREATE TYPE MeshType AS ENUM (
    -- 1次メッシュ:4桁
    '1st_mesh',
    -- 2次メッシュ:6桁
    '2nd_mesh',
    -- 3次メッシュ:8桁
    '3rd_mesh',
    -- 1/2メッシュ/4次メッシュ(500m):9桁
    '1/2_mesh',
    -- 1/4メッシュ/5次メッシュ(250m):10桁
    '1/4_mesh',
    -- 1/8メッシュ/6次メッシュ(125m):11桁
    '1/8_mesh',
    -- 3次メッシュの1/10細分区間(100m):10桁
    -- auto detect対象外
    '1/10_mesh_10char',
    -- 3次メッシュの1/20細分区間(50m):11桁
    -- auto detect対象外
    '1/20_mesh_11char',
    -- 3次メッシュの1/40細分区間(25m):12桁
    '1/40_mesh_12char',
    -- 1/10メッシュ(100m):13桁
    '1/10_mesh',
    -- 1/20メッシュ(50m):13桁
    '1/20_mesh',
    -- 1/25メッシュ(40m):13桁
    '1/25_mesh',
    -- 1/40メッシュ(25m):13桁
    '1/40_mesh',
    -- 1/50メッシュ(20m):13桁
    '1/50_mesh',
    -- 1/80メッシュ(12.5m):13桁
    '1/80_mesh',
    -- 1/100メッシュ(10m):13桁
    '1/100_mesh',
    -- 1/200メッシュ(5m):15桁
    '1/200_mesh',
    -- 1/250メッシュ(4m):15桁
    '1/250_mesh',
    -- 1/400メッシュ(2.5m):15桁
    '1/400_mesh',
    -- 1/500メッシュ(2m):15桁
    '1/500_mesh',
    -- 1/800メッシュ(1.25m):15桁
    '1/800_mesh',
    -- 1/1000メッシュ(1m):15桁
    '1/1000_mesh',
    -- 不明
    'unknown'
    );

-- メッシュコードからメッシュタイプを取得
-- 以下は対象外
-- * 3次メッシュの1/10細分区間(100m):10桁
-- * 3次メッシュの1/20細分区間(50m):11桁
-- https://www.mlit.go.jp/common/001097667.pdf p.15 より
DROP FUNCTION IF EXISTS get_mesh_type(meshcode TEXT) CASCADE;
CREATE FUNCTION get_mesh_type(meshcode TEXT)
    RETURNS MeshType AS
$$
DECLARE
    mesh_length   INT := LENGTH(meshcode);
    div_indicator TEXT;
BEGIN
    IF mesh_length = 4 THEN
        RETURN '1st_mesh';
    END IF;
    IF mesh_length = 6 THEN
        RETURN '2nd_mesh';
    END IF;
    IF mesh_length = 8 THEN
        RETURN '3rd_mesh';
    END IF;
    IF mesh_length = 9 THEN
        RETURN '1/2_mesh';
    END IF;
    IF mesh_length = 10 THEN
        RETURN '1/4_mesh';
    END IF;
    IF mesh_length = 11 THEN
        RETURN '1/8_mesh';
    END IF;
    IF mesh_length = 12 THEN
        RETURN '1/40_mesh_12char';
    END IF;

    -- 13桁メッシュコード
    IF mesh_length = 13 THEN
        div_indicator := SUBSTRING(meshcode FROM 9 FOR 1);
        IF div_indicator = '1' THEN
            RETURN '1/10_mesh';
        END IF;
        IF div_indicator = '2' THEN
            RETURN '1/20_mesh';
        END IF;
        IF div_indicator = '5' THEN
            RETURN '1/25_mesh';
        END IF;
        IF div_indicator = '3' THEN
            RETURN '1/40_mesh';
        END IF;
        IF div_indicator = '6' THEN
            RETURN '1/50_mesh';
        END IF;
        IF div_indicator = '4' THEN
            RETURN '1/80_mesh';
        END IF;
        IF div_indicator = '7' THEN
            RETURN '1/100_mesh';
        END IF;
        RETURN 'unknown';
    END IF;

    -- 15桁メッシュコード
    IF mesh_length = 15 THEN
        div_indicator := SUBSTRING(meshcode FROM 9 FOR 1);
        IF div_indicator = '2' THEN
            RETURN '1/200_mesh';
        END IF;
        IF div_indicator = '5' THEN
            RETURN '1/250_mesh';
        END IF;
        IF div_indicator = '3' THEN
            RETURN '1/400_mesh';
        END IF;
        IF div_indicator = '6' THEN
            RETURN '1/500_mesh';
        END IF;
        IF div_indicator = '4' THEN
            RETURN '1/800_mesh';
        END IF;
        IF div_indicator = '7' THEN
            RETURN '1/1000_mesh';
        END IF;
        RETURN 'unknown';
    END IF;

    RETURN 'unknown';
END
$$ LANGUAGE plpgsql;

-- メッシュコードからポリゴンを生成
DROP FUNCTION IF EXISTS get_polygon_from_meshcode(meshcode TEXT, srid INT, mesh_type MeshType) CASCADE;
CREATE OR REPLACE FUNCTION get_polygon_from_meshcode(meshcode TEXT, srid INT DEFAULT 4326, mesh_type MeshType DEFAULT NULL)
    RETURNS geometry AS
$$
DECLARE
    meshtype      MeshType;
    -- 計算は桁落ちを抑えるためミリ秒単位で行う
    degree_unit   DOUBLE PRECISION := 3600000;
    origin_left   DOUBLE PRECISION := 100 * degree_unit;
    w_1st_mesh    DOUBLE PRECISION := 1 * degree_unit;
    h_1st_mesh    DOUBLE PRECISION := degree_unit / 1.5;
    w_2nd_mesh    DOUBLE PRECISION := w_1st_mesh / 8;
    h_2nd_mesh    DOUBLE PRECISION := h_1st_mesh / 8;
    w_3rd_mesh    DOUBLE PRECISION := w_2nd_mesh / 10;
    h_3rd_mesh    DOUBLE PRECISION := h_2nd_mesh / 10;
    w_1_2_mesh    DOUBLE PRECISION := w_3rd_mesh / 2;
    h_1_2_mesh    DOUBLE PRECISION := h_3rd_mesh / 2;
    w_1_4_mesh    DOUBLE PRECISION := w_3rd_mesh / 4;
    h_1_4_mesh    DOUBLE PRECISION := h_3rd_mesh / 4;
    w_1_8_mesh    DOUBLE PRECISION := w_3rd_mesh / 8;
    h_1_8_mesh    DOUBLE PRECISION := h_3rd_mesh / 8;
    w_1_10_mesh   DOUBLE PRECISION := w_3rd_mesh / 10;
    h_1_10_mesh   DOUBLE PRECISION := h_3rd_mesh / 10;
    w_1_20_mesh   DOUBLE PRECISION := w_3rd_mesh / 20;
    h_1_20_mesh   DOUBLE PRECISION := h_3rd_mesh / 20;
    w_1_25_mesh   DOUBLE PRECISION := w_3rd_mesh / 25;
    h_1_25_mesh   DOUBLE PRECISION := h_3rd_mesh / 25;
    w_1_40_mesh   DOUBLE PRECISION := w_3rd_mesh / 40;
    h_1_40_mesh   DOUBLE PRECISION := h_3rd_mesh / 40;
    w_1_50_mesh   DOUBLE PRECISION := w_3rd_mesh / 50;
    h_1_50_mesh   DOUBLE PRECISION := h_3rd_mesh / 50;
    w_1_80_mesh   DOUBLE PRECISION := w_3rd_mesh / 80;
    h_1_80_mesh   DOUBLE PRECISION := h_3rd_mesh / 80;
    w_1_100_mesh  DOUBLE PRECISION := w_3rd_mesh / 100;
    h_1_100_mesh  DOUBLE PRECISION := h_3rd_mesh / 100;
    w_1_200_mesh  DOUBLE PRECISION := w_3rd_mesh / 200;
    h_1_200_mesh  DOUBLE PRECISION := h_3rd_mesh / 200;
    w_1_250_mesh  DOUBLE PRECISION := w_3rd_mesh / 250;
    h_1_250_mesh  DOUBLE PRECISION := h_3rd_mesh / 250;
    w_1_400_mesh  DOUBLE PRECISION := w_3rd_mesh / 400;
    h_1_400_mesh  DOUBLE PRECISION := h_3rd_mesh / 400;
    w_1_500_mesh  DOUBLE PRECISION := w_3rd_mesh / 500;
    h_1_500_mesh  DOUBLE PRECISION := h_3rd_mesh / 500;
    w_1_800_mesh  DOUBLE PRECISION := w_3rd_mesh / 800;
    h_1_800_mesh  DOUBLE PRECISION := h_3rd_mesh / 800;
    w_1_1000_mesh DOUBLE PRECISION := w_3rd_mesh / 1000;
    h_1_1000_mesh DOUBLE PRECISION := h_3rd_mesh / 1000;
    mesh_left     DOUBLE PRECISION;
    mesh_bottom   DOUBLE PRECISION;
    code1_2       SMALLINT;
    code3_4       SMALLINT;
    code5         SMALLINT;
    code6         SMALLINT;
    code7         SMALLINT;
    code8         SMALLINT;
    code9         SMALLINT;
    code10        SMALLINT;
    code11        SMALLINT;
    code12        SMALLINT;
    code10_11     SMALLINT;
    code12_13     SMALLINT;
    code10_11_12  SMALLINT;
    code13_14_15  SMALLINT;
BEGIN
    IF mesh_type IS NULL THEN
        meshtype := get_mesh_type(meshcode);
    ELSE
        meshtype := mesh_type;
    END IF;
    IF meshtype = 'unknown' THEN
        RETURN NULL;
    END IF;

    -- 1次メッシュ
    code1_2 := CAST(SUBSTRING(meshcode FROM 1 FOR 2) AS SMALLINT);
    code3_4 := CAST(SUBSTRING(meshcode FROM 3 FOR 2) AS SMALLINT);

    mesh_left := origin_left + code3_4 * w_1st_mesh;
    mesh_bottom := code1_2 * h_1st_mesh;

    IF meshtype = '1st_mesh' THEN
        RETURN ST_MakeEnvelope(
                mesh_left / degree_unit, mesh_bottom / degree_unit,
                (mesh_left + w_1st_mesh) / degree_unit, (mesh_bottom + h_1st_mesh) / degree_unit,
                srid);
    END IF;

    -- 2次メッシュ
    code5 := CAST(SUBSTRING(meshcode FROM 5 FOR 1) AS SMALLINT);
    code6 := CAST(SUBSTRING(meshcode FROM 6 FOR 1) AS SMALLINT);

    mesh_left := mesh_left + code6 * w_2nd_mesh;
    mesh_bottom := mesh_bottom + code5 * h_2nd_mesh;

    IF meshtype = '2nd_mesh' THEN
        RETURN ST_MakeEnvelope(
                mesh_left / degree_unit, mesh_bottom / degree_unit,
                (mesh_left + w_2nd_mesh) / degree_unit, (mesh_bottom + h_2nd_mesh) / degree_unit,
                srid);
    END IF;

    -- 3次メッシュ
    code7 := CAST(SUBSTRING(meshcode FROM 7 FOR 1) AS SMALLINT);
    code8 := CAST(SUBSTRING(meshcode FROM 8 FOR 1) AS SMALLINT);

    mesh_left := mesh_left + code8 * w_3rd_mesh;
    mesh_bottom := mesh_bottom + code7 * h_3rd_mesh;

    IF meshtype = '3rd_mesh' THEN
        RETURN ST_MakeEnvelope(
                mesh_left / degree_unit, mesh_bottom / degree_unit,
                (mesh_left + w_3rd_mesh) / degree_unit, (mesh_bottom + h_3rd_mesh) / degree_unit,
                srid);
    END IF;

    -- 1/8メッシュまでは4分割していく
    IF meshtype <= '1/8_mesh' THEN
        -- 1/2メッシュ (4分割タイプ)
        code9 := CAST(SUBSTRING(meshcode FROM 9 FOR 1) AS SMALLINT);

        IF code9 = 1 THEN
        END IF;
        IF code9 = 2 THEN
            mesh_left := mesh_left + w_1_2_mesh;
        END IF;
        IF code9 = 3 THEN
            mesh_bottom := mesh_bottom + h_1_2_mesh;
        END IF;
        IF code9 = 4 THEN
            mesh_left := mesh_left + w_1_2_mesh;
            mesh_bottom := mesh_bottom + h_1_2_mesh;
        END IF;

        IF meshtype = '1/2_mesh' THEN
            RETURN ST_MakeEnvelope(
                    mesh_left / degree_unit, mesh_bottom / degree_unit,
                    (mesh_left + w_1_2_mesh) / degree_unit, (mesh_bottom + h_1_2_mesh) / degree_unit,
                    srid);
        END IF;

        -- 1/4メッシュ (4分割タイプ)
        code10 := CAST(SUBSTRING(meshcode FROM 10 FOR 1) AS SMALLINT);

        IF code10 = 1 THEN
        END IF;
        IF code10 = 2 THEN
            mesh_left := mesh_left + w_1_4_mesh;
        END IF;
        IF code10 = 3 THEN
            mesh_bottom := mesh_bottom + h_1_4_mesh;
        END IF;
        IF code10 = 4 THEN
            mesh_left := mesh_left + w_1_4_mesh;
            mesh_bottom := mesh_bottom + h_1_4_mesh;
        END IF;

        IF meshtype = '1/4_mesh' THEN
            RETURN ST_MakeEnvelope(
                    mesh_left / degree_unit, mesh_bottom / degree_unit,
                    (mesh_left + w_1_4_mesh) / degree_unit, (mesh_bottom + h_1_4_mesh) / degree_unit,
                    srid);
        END IF;

        -- 1/8メッシュ (4分割タイプ)
        code11 := CAST(SUBSTRING(meshcode FROM 11 FOR 1) AS SMALLINT);

        IF code11 = 1 THEN
        END IF;
        IF code11 = 2 THEN
            mesh_left := mesh_left + w_1_8_mesh;
        END IF;
        IF code11 = 3 THEN
            mesh_bottom := mesh_bottom + h_1_8_mesh;
        END IF;
        IF code11 = 4 THEN
            mesh_left := mesh_left + w_1_8_mesh;
            mesh_bottom := mesh_bottom + h_1_8_mesh;
        END IF;

        IF meshtype = '1/8_mesh' THEN
            RETURN ST_MakeEnvelope(
                    mesh_left / degree_unit, mesh_bottom / degree_unit,
                    (mesh_left + w_1_8_mesh) / degree_unit, (mesh_bottom + h_1_8_mesh) / degree_unit,
                    srid);
        END IF;

        RETURN NULL;
    END IF;

    -- 3次メッシュの細分区画
    IF meshtype <= '1/40_mesh_12char' THEN
        code9 := CAST(SUBSTRING(meshcode FROM 9 FOR 1) AS SMALLINT);
        code10 := CAST(SUBSTRING(meshcode FROM 10 FOR 1) AS SMALLINT);

        mesh_left := mesh_left + code10 * w_1_10_mesh;
        mesh_bottom := mesh_bottom + code9 * h_1_10_mesh;

        -- 1/10メッシュの細分区画
        IF meshtype = '1/10_mesh_10char' THEN
            RETURN ST_MakeEnvelope(
                    mesh_left / degree_unit, mesh_bottom / degree_unit,
                    (mesh_left + w_1_10_mesh) / degree_unit, (mesh_bottom + h_1_10_mesh) / degree_unit,
                    srid);
        END IF;

        IF meshtype <= '1/40_mesh_12char' THEN
            -- 1/20メッシュの細分区画(4分割タイプ)
            code11 := CAST(SUBSTRING(meshcode FROM 11 FOR 1) AS SMALLINT);

            IF code11 = 1 THEN
            END IF;
            IF code11 = 2 THEN
                mesh_left := mesh_left + w_1_20_mesh;
            END IF;
            IF code11 = 3 THEN
                mesh_bottom := mesh_bottom + h_1_20_mesh;
            END IF;
            IF code11 = 4 THEN
                mesh_left := mesh_left + w_1_20_mesh;
                mesh_bottom := mesh_bottom + h_1_20_mesh;
            END IF;

            IF meshtype = '1/20_mesh_11char' THEN
                RETURN ST_MakeEnvelope(
                        mesh_left / degree_unit, mesh_bottom / degree_unit,
                        (mesh_left + w_1_20_mesh) / degree_unit, (mesh_bottom + h_1_20_mesh) / degree_unit,
                        srid);
            END IF;

            -- 1/40メッシュの細分区画
            code12 := CAST(SUBSTRING(meshcode FROM 12 FOR 1) AS SMALLINT);

            IF code12 = 1 THEN
            END IF;
            IF code12 = 2 THEN
                mesh_left := mesh_left + w_1_40_mesh;
            END IF;
            IF code12 = 3 THEN
                mesh_bottom := mesh_bottom + h_1_40_mesh;
            END IF;
            IF code12 = 4 THEN
                mesh_left := mesh_left + w_1_40_mesh;
                mesh_bottom := mesh_bottom + h_1_40_mesh;
            END IF;

            IF meshtype = '1/40_mesh_12char' THEN
                RETURN ST_MakeEnvelope(
                        mesh_left / degree_unit, mesh_bottom / degree_unit,
                        (mesh_left + w_1_40_mesh) / degree_unit, (mesh_bottom + h_1_40_mesh) / degree_unit,
                        srid);
            END IF;

        END IF;

        RETURN NULL;
    END IF;

    -- 1/100メッシュまでは13桁メッシュコード
    IF meshtype <= '1/100_mesh' THEN
        -- 1/10メッシュ
        IF meshtype = '1/10_mesh' THEN
            code10_11 := CAST(SUBSTRING(meshcode FROM 10 FOR 2) AS SMALLINT);
            code12_13 := CAST(SUBSTRING(meshcode FROM 12 FOR 2) AS SMALLINT);

            mesh_left := mesh_left + code12_13 * w_1_10_mesh;
            mesh_bottom := mesh_bottom + code10_11 * h_1_10_mesh;

            RETURN ST_MakeEnvelope(
                    mesh_left / degree_unit, mesh_bottom / degree_unit,
                    (mesh_left + w_1_10_mesh) / degree_unit, (mesh_bottom + h_1_10_mesh) / degree_unit,
                    srid);
        END IF;

        -- 1/20メッシュ
        IF meshtype = '1/20_mesh' THEN
            code10_11 := CAST(SUBSTRING(meshcode FROM 10 FOR 2) AS SMALLINT);
            code12_13 := CAST(SUBSTRING(meshcode FROM 12 FOR 2) AS SMALLINT);

            mesh_left := mesh_left + code12_13 * w_1_20_mesh;
            mesh_bottom := mesh_bottom + code10_11 * h_1_20_mesh;

            RETURN ST_MakeEnvelope(
                    mesh_left / degree_unit, mesh_bottom / degree_unit,
                    (mesh_left + w_1_20_mesh) / degree_unit, (mesh_bottom + h_1_20_mesh) / degree_unit,
                    srid);
        END IF;

        -- 1/25メッシュ
        IF meshtype = '1/25_mesh' THEN
            code10_11 := CAST(SUBSTRING(meshcode FROM 10 FOR 2) AS SMALLINT);
            code12_13 := CAST(SUBSTRING(meshcode FROM 12 FOR 2) AS SMALLINT);

            mesh_left := mesh_left + code12_13 * w_1_25_mesh;
            mesh_bottom := mesh_bottom + code10_11 * h_1_25_mesh;

            RETURN ST_MakeEnvelope(
                    mesh_left / degree_unit, mesh_bottom / degree_unit,
                    (mesh_left + w_1_25_mesh) / degree_unit, (mesh_bottom + h_1_25_mesh) / degree_unit,
                    srid);
        END IF;

        -- 1/40メッシュ
        IF meshtype = '1/40_mesh' THEN
            code10_11 := CAST(SUBSTRING(meshcode FROM 10 FOR 2) AS SMALLINT);
            code12_13 := CAST(SUBSTRING(meshcode FROM 12 FOR 2) AS SMALLINT);

            mesh_left := mesh_left + code12_13 * w_1_40_mesh;
            mesh_bottom := mesh_bottom + code10_11 * h_1_40_mesh;

            RETURN ST_MakeEnvelope(
                    mesh_left / degree_unit, mesh_bottom / degree_unit,
                    (mesh_left + w_1_40_mesh) / degree_unit, (mesh_bottom + h_1_40_mesh) / degree_unit,
                    srid);
        END IF;

        -- 1/50メッシュ
        IF meshtype = '1/50_mesh' THEN
            code10_11 := CAST(SUBSTRING(meshcode FROM 10 FOR 2) AS SMALLINT);
            code12_13 := CAST(SUBSTRING(meshcode FROM 12 FOR 2) AS SMALLINT);

            mesh_left := mesh_left + code12_13 * w_1_50_mesh;
            mesh_bottom := mesh_bottom + code10_11 * h_1_50_mesh;

            RETURN ST_MakeEnvelope(
                    mesh_left / degree_unit, mesh_bottom / degree_unit,
                    (mesh_left + w_1_50_mesh) / degree_unit, (mesh_bottom + h_1_50_mesh) / degree_unit,
                    srid);
        END IF;

        -- 1/80メッシュ
        IF meshtype = '1/80_mesh' THEN
            code10_11 := CAST(SUBSTRING(meshcode FROM 10 FOR 2) AS SMALLINT);
            code12_13 := CAST(SUBSTRING(meshcode FROM 12 FOR 2) AS SMALLINT);

            mesh_left := mesh_left + code12_13 * w_1_80_mesh;
            mesh_bottom := mesh_bottom + code10_11 * h_1_80_mesh;

            RETURN ST_MakeEnvelope(
                    mesh_left / degree_unit, mesh_bottom / degree_unit,
                    (mesh_left + w_1_80_mesh) / degree_unit, (mesh_bottom + h_1_80_mesh) / degree_unit,
                    srid);
        END IF;

        -- 1/100メッシュ
        IF meshtype = '1/100_mesh' THEN
            code10_11 := CAST(SUBSTRING(meshcode FROM 10 FOR 2) AS SMALLINT);
            code12_13 := CAST(SUBSTRING(meshcode FROM 12 FOR 2) AS SMALLINT);

            mesh_left := mesh_left + code12_13 * w_1_100_mesh;
            mesh_bottom := mesh_bottom + code10_11 * h_1_100_mesh;

            RETURN ST_MakeEnvelope(
                    mesh_left / degree_unit, mesh_bottom / degree_unit,
                    (mesh_left + w_1_100_mesh) / degree_unit, (mesh_bottom + h_1_100_mesh) / degree_unit,
                    srid);
        END IF;

        RETURN NULL;
    END IF;

    -- 1/1000メッシュまでは15桁メッシュコード
    IF meshtype <= '1/1000_mesh' THEN
        -- 1/200メッシュ
        IF meshtype = '1/200_mesh' THEN
            code10_11_12 := CAST(SUBSTRING(meshcode FROM 10 FOR 3) AS SMALLINT);
            code13_14_15 := CAST(SUBSTRING(meshcode FROM 13 FOR 3) AS SMALLINT);

            mesh_left := mesh_left + code13_14_15 * w_1_200_mesh;
            mesh_bottom := mesh_bottom + code10_11_12 * h_1_200_mesh;

            RETURN ST_MakeEnvelope(
                    mesh_left / degree_unit, mesh_bottom / degree_unit,
                    (mesh_left + w_1_200_mesh) / degree_unit, (mesh_bottom + h_1_200_mesh) / degree_unit,
                    srid);
        END IF;

        -- 1/250メッシュ
        IF meshtype = '1/250_mesh' THEN
            code10_11_12 := CAST(SUBSTRING(meshcode FROM 10 FOR 3) AS SMALLINT);
            code13_14_15 := CAST(SUBSTRING(meshcode FROM 13 FOR 3) AS SMALLINT);

            mesh_left := mesh_left + code13_14_15 * w_1_250_mesh;
            mesh_bottom := mesh_bottom + code10_11_12 * h_1_250_mesh;

            RETURN ST_MakeEnvelope(
                    mesh_left / degree_unit, mesh_bottom / degree_unit,
                    (mesh_left + w_1_250_mesh) / degree_unit, (mesh_bottom + h_1_250_mesh) / degree_unit,
                    srid);
        END IF;

        -- 1/400メッシュ
        IF meshtype = '1/400_mesh' THEN
            code10_11_12 := CAST(SUBSTRING(meshcode FROM 10 FOR 3) AS SMALLINT);
            code13_14_15 := CAST(SUBSTRING(meshcode FROM 13 FOR 3) AS SMALLINT);

            mesh_left := mesh_left + code13_14_15 * w_1_400_mesh;
            mesh_bottom := mesh_bottom + code10_11_12 * h_1_400_mesh;

            RETURN ST_MakeEnvelope(
                    mesh_left / degree_unit, mesh_bottom / degree_unit,
                    (mesh_left + w_1_400_mesh) / degree_unit, (mesh_bottom + h_1_400_mesh) / degree_unit,
                    srid);
        END IF;

        -- 1/500メッシュ
        IF meshtype = '1/500_mesh' THEN
            code10_11_12 := CAST(SUBSTRING(meshcode FROM 10 FOR 3) AS SMALLINT);
            code13_14_15 := CAST(SUBSTRING(meshcode FROM 13 FOR 3) AS SMALLINT);

            mesh_left := mesh_left + code13_14_15 * w_1_500_mesh;
            mesh_bottom := mesh_bottom + code10_11_12 * h_1_500_mesh;

            RETURN ST_MakeEnvelope(
                    mesh_left / degree_unit, mesh_bottom / degree_unit,
                    (mesh_left + w_1_500_mesh) / degree_unit, (mesh_bottom + h_1_500_mesh) / degree_unit,
                    srid);
        END IF;

        -- 1/800メッシュ
        IF meshtype = '1/800_mesh' THEN
            code10_11_12 := CAST(SUBSTRING(meshcode FROM 10 FOR 3) AS SMALLINT);
            code13_14_15 := CAST(SUBSTRING(meshcode FROM 13 FOR 3) AS SMALLINT);

            mesh_left := mesh_left + code13_14_15 * w_1_800_mesh;
            mesh_bottom := mesh_bottom + code10_11_12 * h_1_800_mesh;

            RETURN ST_MakeEnvelope(
                    mesh_left / degree_unit, mesh_bottom / degree_unit,
                    (mesh_left + w_1_800_mesh) / degree_unit, (mesh_bottom + h_1_800_mesh) / degree_unit,
                    srid);
        END IF;

        -- 1/1000メッシュ
        IF meshtype = '1/1000_mesh' THEN
            code10_11_12 := CAST(SUBSTRING(meshcode FROM 10 FOR 3) AS SMALLINT);
            code13_14_15 := CAST(SUBSTRING(meshcode FROM 13 FOR 3) AS SMALLINT);

            mesh_left := mesh_left + code13_14_15 * w_1_1000_mesh;
            mesh_bottom := mesh_bottom + code10_11_12 * h_1_1000_mesh;

            RETURN ST_MakeEnvelope(
                    mesh_left / degree_unit, mesh_bottom / degree_unit,
                    (mesh_left + w_1_1000_mesh) / degree_unit, (mesh_bottom + h_1_1000_mesh) / degree_unit,
                    srid);
        END IF;

        RETURN NULL;
    END IF;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS get_meshcode_from_point(lon DOUBLE PRECISION, lat DOUBLE PRECISION, meshtype MeshType);
CREATE FUNCTION get_meshcode_from_point(
    lon DOUBLE PRECISION,
    lat DOUBLE PRECISION,
    meshtype MeshType DEFAULT NULL
)
    RETURNS TEXT
AS
$$
DECLARE
    mesh_code          TEXT             := NULL;
    x                  DOUBLE PRECISION;
    y                  DOUBLE PRECISION;
    origin_left        DOUBLE PRECISION := 100 * 3600;
    w_1st_mesh         DOUBLE PRECISION := 1 * 3600;
    h_1st_mesh         DOUBLE PRECISION := 3600 / 1.5;
    w_2nd_mesh         DOUBLE PRECISION := w_1st_mesh / 8;
    h_2nd_mesh         DOUBLE PRECISION := h_1st_mesh / 8;
    w_3rd_mesh         DOUBLE PRECISION := w_2nd_mesh / 10;
    h_3rd_mesh         DOUBLE PRECISION := h_2nd_mesh / 10;
    w_1_2_mesh         DOUBLE PRECISION := w_3rd_mesh / 2;
    h_1_2_mesh         DOUBLE PRECISION := h_3rd_mesh / 2;
    w_1_4_mesh         DOUBLE PRECISION := w_3rd_mesh / 4;
    h_1_4_mesh         DOUBLE PRECISION := h_3rd_mesh / 4;
    w_1_8_mesh         DOUBLE PRECISION := w_3rd_mesh / 8;
    h_1_8_mesh         DOUBLE PRECISION := h_3rd_mesh / 8;
    w_1_10_mesh        DOUBLE PRECISION := w_3rd_mesh / 10;
    h_1_10_mesh        DOUBLE PRECISION := h_3rd_mesh / 10;
    w_1_20_mesh        DOUBLE PRECISION := w_3rd_mesh / 20;
    h_1_20_mesh        DOUBLE PRECISION := h_3rd_mesh / 20;
    w_1_25_mesh        DOUBLE PRECISION := w_3rd_mesh / 25;
    h_1_25_mesh        DOUBLE PRECISION := h_3rd_mesh / 25;
    w_1_40_mesh        DOUBLE PRECISION := w_3rd_mesh / 40;
    h_1_40_mesh        DOUBLE PRECISION := h_3rd_mesh / 40;
    w_1_50_mesh        DOUBLE PRECISION := w_3rd_mesh / 50;
    h_1_50_mesh        DOUBLE PRECISION := h_3rd_mesh / 50;
    w_1_80_mesh        DOUBLE PRECISION := w_3rd_mesh / 80;
    h_1_80_mesh        DOUBLE PRECISION := h_3rd_mesh / 80;
    w_1_100_mesh       DOUBLE PRECISION := w_3rd_mesh / 100;
    h_1_100_mesh       DOUBLE PRECISION := h_3rd_mesh / 100;
    w_1_200_mesh       DOUBLE PRECISION := w_3rd_mesh / 200;
    h_1_200_mesh       DOUBLE PRECISION := h_3rd_mesh / 200;
    w_1_250_mesh       DOUBLE PRECISION := w_3rd_mesh / 250;
    h_1_250_mesh       DOUBLE PRECISION := h_3rd_mesh / 250;
    w_1_400_mesh       DOUBLE PRECISION := w_3rd_mesh / 400;
    h_1_400_mesh       DOUBLE PRECISION := h_3rd_mesh / 400;
    w_1_500_mesh       DOUBLE PRECISION := w_3rd_mesh / 500;
    h_1_500_mesh       DOUBLE PRECISION := h_3rd_mesh / 500;
    w_1_800_mesh       DOUBLE PRECISION := w_3rd_mesh / 800;
    h_1_800_mesh       DOUBLE PRECISION := h_3rd_mesh / 800;
    w_1_1000_mesh      DOUBLE PRECISION := w_3rd_mesh / 1000;
    h_1_1000_mesh      DOUBLE PRECISION := h_3rd_mesh / 1000;
    h_1_10_indicator   SMALLINT         := 1;
    h_1_20_indicator   SMALLINT         := 2;
    h_1_25_indicator   SMALLINT         := 5;
    h_1_40_indicator   SMALLINT         := 3;
    h_1_50_indicator   SMALLINT         := 6;
    h_1_80_indicator   SMALLINT         := 4;
    h_1_100_indicator  SMALLINT         := 7;
    h_1_200_indicator  SMALLINT         := 2;
    h_1_250_indicator  SMALLINT         := 5;
    h_1_400_indicator  SMALLINT         := 3;
    h_1_500_indicator  SMALLINT         := 6;
    h_1_800_indicator  SMALLINT         := 4;
    h_1_1000_indicator SMALLINT         := 7;
    code1_2            SMALLINT;
    code3_4            SMALLINT;
    code5              SMALLINT;
    code6              SMALLINT;
    code7              SMALLINT;
    code8              SMALLINT;
    code9              SMALLINT;
    code10             SMALLINT;
    code11             SMALLINT;
    code12             SMALLINT;
    code10_11          SMALLINT;
    code12_13          SMALLINT;
    code10_11_12       SMALLINT;
    code13_14_15       SMALLINT;
    quarter_x          SMALLINT;
    quarter_y          SMALLINT;
BEGIN
    x := lon * 3600 - origin_left;
    y := lat * 3600;

    -- 1次メッシュ
    code1_2 := FLOOR(y / h_1st_mesh);
    code3_4 := FLOOR(x / w_1st_mesh);
    y := y - code1_2 * h_1st_mesh;
    x := x - code3_4 * w_1st_mesh;
    mesh_code := LPAD(code1_2::TEXT, 2, '0') || LPAD(code3_4::TEXT, 2, '0');

    IF meshtype = '1st_mesh' THEN
        RETURN mesh_code;
    END IF;

    -- 2次メッシュ
    code5 := FLOOR(y / h_2nd_mesh);
    code6 := FLOOR(x / w_2nd_mesh);
    y := y - code5 * h_2nd_mesh;
    x := x - code6 * w_2nd_mesh;
    mesh_code := mesh_code || code5::TEXT || code6::TEXT;

    IF meshtype = '2nd_mesh' THEN
        RETURN mesh_code;
    END IF;

    -- 3次メッシュ
    code7 := FLOOR(y / h_3rd_mesh);
    code8 := FLOOR(x / w_3rd_mesh);
    y := y - code7 * h_3rd_mesh;
    x := x - code8 * w_3rd_mesh;
    mesh_code := mesh_code || code7::TEXT || code8::TEXT;

    IF meshtype = '3rd_mesh' THEN
        RETURN mesh_code;
    END IF;

    -- 1/8メッシュまで
    IF meshtype <= '1/8_mesh' THEN
        -- 1/2メッシュ
        quarter_y := FLOOR(y / h_1_2_mesh);
        quarter_x := FLOOR(x / w_1_2_mesh);
        y := y - quarter_y * h_1_2_mesh;
        x := x - quarter_x * w_1_2_mesh;
        code9 := (quarter_x + 1) + (quarter_y * 2);
        mesh_code := mesh_code || code9::TEXT;

        IF meshtype = '1/2_mesh' THEN
            RETURN mesh_code;
        END IF;

        -- 1/4メッシュ
        quarter_y := FLOOR(y / h_1_4_mesh);
        quarter_x := FLOOR(x / w_1_4_mesh);
        y := y - quarter_y * h_1_4_mesh;
        x := x - quarter_x * w_1_4_mesh;
        code10 := (quarter_x + 1) + (quarter_y * 2);
        mesh_code := mesh_code || code10::TEXT;

        IF meshtype = '1/4_mesh' THEN
            RETURN mesh_code;
        END IF;

        -- 1/8メッシュ
        quarter_y := FLOOR(y / h_1_8_mesh);
        quarter_x := FLOOR(x / w_1_8_mesh);
        y := y - quarter_y * h_1_8_mesh;
        x := x - quarter_x * w_1_8_mesh;
        code11 := (quarter_x + 1) + (quarter_y * 2);
        mesh_code := mesh_code || code11::TEXT;

        IF meshtype = '1/8_mesh' THEN
            RETURN mesh_code;
        END IF;

        RETURN NULL;
    END IF;

    -- 3次メッシュの細分区画
    IF meshtype <= '1/40_mesh_12char' THEN
        -- 1/10メッシュの細分区画
        code9 := FLOOR(y / h_1_10_mesh);
        code10 := FLOOR(x / w_1_10_mesh);
        y := y - code9 * h_1_10_mesh;
        x := x - code10 * w_1_10_mesh;
        mesh_code := mesh_code || code9::TEXT || code10::TEXT;

        IF meshtype = '1/10_mesh_10char' THEN
            RETURN mesh_code;
        END IF;

        -- 1/20メッシュの細分区画
        quarter_y := FLOOR(y / h_1_20_mesh);
        quarter_x := FLOOR(x / w_1_20_mesh);
        y := y - quarter_y * h_1_20_mesh;
        x := x - quarter_x * w_1_20_mesh;
        code11 := (quarter_x + 1) + (quarter_y * 2);
        mesh_code := mesh_code || code11::TEXT;

        IF meshtype = '1/20_mesh_11char' THEN
            RETURN mesh_code;
        END IF;

        -- 1/40メッシュの細分区画
        quarter_y := FLOOR(y / h_1_40_mesh);
        quarter_x := FLOOR(x / w_1_40_mesh);
        y := y - quarter_y * h_1_40_mesh;
        x := x - quarter_x * w_1_40_mesh;
        code12 := (quarter_x + 1) + (quarter_y * 2);
        mesh_code := mesh_code || code12::TEXT;

        IF meshtype = '1/40_mesh_12char' THEN
            RETURN mesh_code;
        END IF;

        RETURN NULL;
    END IF;

    -- 13桁メッシュ
    IF meshtype <= '1/100_mesh' THEN
        IF meshtype = '1/10_mesh' THEN
            code10_11 := FLOOR(y / h_1_10_mesh);
            code12_13 := FLOOR(x / w_1_10_mesh);
            y := y - code10_11 * h_1_10_mesh;
            x := x - code12_13 * w_1_10_mesh;
            mesh_code := mesh_code || h_1_10_indicator::TEXT || LPAD(code10_11::TEXT, 2, '0') ||
                         LPAD(code12_13::TEXT, 2, '0');
        END IF;

        IF meshtype = '1/20_mesh' THEN
            code10_11 := FLOOR(y / h_1_20_mesh);
            code12_13 := FLOOR(x / w_1_20_mesh);
            y := y - code10_11 * h_1_20_mesh;
            x := x - code12_13 * w_1_20_mesh;
            mesh_code := mesh_code || h_1_20_indicator::TEXT || LPAD(code10_11::TEXT, 2, '0') ||
                         LPAD(code12_13::TEXT, 2, '0');
        END IF;

        IF meshtype = '1/25_mesh' THEN
            code10_11 := FLOOR(y / h_1_25_mesh);
            code12_13 := FLOOR(x / w_1_25_mesh);
            y := y - code10_11 * h_1_25_mesh;
            x := x - code12_13 * w_1_25_mesh;
            mesh_code := mesh_code || h_1_25_indicator::TEXT || LPAD(code10_11::TEXT, 2, '0') ||
                         LPAD(code12_13::TEXT, 2, '0');
        END IF;

        IF meshtype = '1/40_mesh' THEN
            code10_11 := FLOOR(y / h_1_40_mesh);
            code12_13 := FLOOR(x / w_1_40_mesh);
            y := y - code10_11 * h_1_40_mesh;
            x := x - code12_13 * w_1_40_mesh;
            mesh_code := mesh_code || h_1_40_indicator::TEXT || LPAD(code10_11::TEXT, 2, '0') ||
                         LPAD(code12_13::TEXT, 2, '0');
        END IF;

        IF meshtype = '1/50_mesh' THEN
            code10_11 := FLOOR(y / h_1_50_mesh);
            code12_13 := FLOOR(x / w_1_50_mesh);
            y := y - code10_11 * h_1_50_mesh;
            x := x - code12_13 * w_1_50_mesh;
            mesh_code := mesh_code || h_1_50_indicator::TEXT || LPAD(code10_11::TEXT, 2, '0') ||
                         LPAD(code12_13::TEXT, 2, '0');
        END IF;

        IF meshtype = '1/80_mesh' THEN
            code10_11 := FLOOR(y / h_1_80_mesh);
            code12_13 := FLOOR(x / w_1_80_mesh);
            y := y - code10_11 * h_1_80_mesh;
            x := x - code12_13 * w_1_80_mesh;
            mesh_code := mesh_code || h_1_80_indicator::TEXT || LPAD(code10_11::TEXT, 2, '0') ||
                         LPAD(code12_13::TEXT, 2, '0');
        END IF;

        IF meshtype = '1/100_mesh' THEN
            code10_11 := FLOOR(y / h_1_100_mesh);
            code12_13 := FLOOR(x / w_1_100_mesh);
            y := y - code10_11 * h_1_100_mesh;
            x := x - code12_13 * w_1_100_mesh;
            mesh_code := mesh_code || h_1_100_indicator::TEXT || LPAD(code10_11::TEXT, 2, '0') ||
                         LPAD(code12_13::TEXT, 2, '0');
        END IF;

        RETURN mesh_code;
    END IF;

    -- 15桁メッシュ
    IF meshtype <= '1/1000_mesh' THEN
        IF meshtype = '1/200_mesh' THEN
            code10_11_12 := FLOOR(y / h_1_200_mesh);
            code13_14_15 := FLOOR(x / w_1_200_mesh);
            y := y - code10_11_12 * h_1_200_mesh;
            x := x - code13_14_15 * w_1_200_mesh;
            mesh_code := mesh_code || h_1_200_indicator::TEXT || LPAD(code10_11_12::TEXT, 3, '0') ||
                         LPAD(code13_14_15::TEXT, 3, '0');
        END IF;

        IF meshtype = '1/250_mesh' THEN
            code10_11_12 := FLOOR(y / h_1_250_mesh);
            code13_14_15 := FLOOR(x / w_1_250_mesh);
            y := y - code10_11_12 * h_1_250_mesh;
            x := x - code13_14_15 * w_1_250_mesh;
            mesh_code := mesh_code || h_1_250_indicator::TEXT || LPAD(code10_11_12::TEXT, 3, '0') ||
                         LPAD(code13_14_15::TEXT, 3, '0');
        END IF;

        IF meshtype = '1/400_mesh' THEN
            code10_11_12 := FLOOR(y / h_1_400_mesh);
            code13_14_15 := FLOOR(x / w_1_400_mesh);
            y := y - code10_11_12 * h_1_400_mesh;
            x := x - code13_14_15 * w_1_400_mesh;
            mesh_code := mesh_code || h_1_400_indicator::TEXT || LPAD(code10_11_12::TEXT, 3, '0') ||
                         LPAD(code13_14_15::TEXT, 3, '0');
        END IF;

        IF meshtype = '1/500_mesh' THEN
            code10_11_12 := FLOOR(y / h_1_500_mesh);
            code13_14_15 := FLOOR(x / w_1_500_mesh);
            y := y - code10_11_12 * h_1_500_mesh;
            x := x - code13_14_15 * w_1_500_mesh;
            mesh_code := mesh_code || h_1_500_indicator::TEXT || LPAD(code10_11_12::TEXT, 3, '0') ||
                         LPAD(code13_14_15::TEXT, 3, '0');
        END IF;

        IF meshtype = '1/800_mesh' THEN
            code10_11_12 := FLOOR(y / h_1_800_mesh);
            code13_14_15 := FLOOR(x / w_1_800_mesh);
            y := y - code10_11_12 * h_1_800_mesh;
            x := x - code13_14_15 * w_1_800_mesh;
            mesh_code := mesh_code || h_1_800_indicator::TEXT || LPAD(code10_11_12::TEXT, 3, '0') ||
                         LPAD(code13_14_15::TEXT, 3, '0');
        END IF;

        IF meshtype = '1/1000_mesh' THEN
            code10_11_12 := FLOOR(y / h_1_1000_mesh);
            code13_14_15 := FLOOR(x / w_1_1000_mesh);
            y := y - code10_11_12 * h_1_1000_mesh;
            x := x - code13_14_15 * w_1_1000_mesh;
            mesh_code := mesh_code || h_1_1000_indicator::TEXT || LPAD(code10_11_12::TEXT, 3, '0') ||
                         LPAD(code13_14_15::TEXT, 3, '0');
        END IF;

        RETURN mesh_code;
    END IF;

    RETURN mesh_code;
END;

$$ LANGUAGE plpgsql;

-- サポートされているメッシュタイプを取得する
DROP FUNCTION IF EXISTS supported_mesh_types();
CREATE OR REPLACE FUNCTION supported_mesh_types()
    RETURNS TABLE
            (
                mesh_type   TEXT,
                description TEXT
            )
AS
$$
WITH mesh_names AS (SELECT '1st_mesh' AS mesh_type, '1次メッシュ:4桁' AS description
                    UNION ALL
                    SELECT '2nd_mesh' AS mesh_type, '2次メッシュ:6桁' AS description
                    UNION ALL
                    SELECT '3rd_mesh' AS mesh_type, '3次メッシュ:8桁' AS description
                    UNION ALL
                    SELECT '1/2_mesh' AS mesh_type, '1/2メッシュ/4次メッシュ(500m):9桁' AS description
                    UNION ALL
                    SELECT '1/4_mesh' AS mesh_type, '1/4メッシュ/5次メッシュ(250m):10桁' AS description
                    UNION ALL
                    SELECT '1/8_mesh' AS mesh_type, '1/8メッシュ/6次メッシュ(125m):11桁' AS description
                    UNION ALL
                    SELECT '1/10_mesh_10char'                                      AS mesh_type,
                           '3次メッシュの1/10細分区間(100m):10桁 **自動検出不可**' AS description
                    UNION ALL
                    SELECT '1/20_mesh_11char'                                     AS mesh_type,
                           '3次メッシュの1/20細分区間(50m):11桁 **自動検出不可**' AS description
                    UNION ALL
                    SELECT '1/40_mesh_12char' AS mesh_type, '3次メッシュの1/40細分区間(25m):12桁' AS description
                    UNION ALL
                    SELECT '1/10_mesh' AS mesh_type, '1/10メッシュ(100m):13桁' AS description
                    UNION ALL
                    SELECT '1/20_mesh' AS mesh_type, '1/20メッシュ(50m):13桁' AS description
                    UNION ALL
                    SELECT '1/25_mesh' AS mesh_type, '1/25メッシュ(40m):13桁' AS description
                    UNION ALL
                    SELECT '1/40_mesh' AS mesh_type, '1/40メッシュ(25m):13桁' AS description
                    UNION ALL
                    SELECT '1/50_mesh' AS mesh_type, '1/50メッシュ(20m):13桁' AS description
                    UNION ALL
                    SELECT '1/80_mesh' AS mesh_type, '1/80メッシュ(12.5m):13桁' AS description
                    UNION ALL
                    SELECT '1/100_mesh' AS mesh_type, '1/100メッシュ(10m):13桁' AS description
                    UNION ALL
                    SELECT '1/200_mesh' AS mesh_type, '1/200メッシュ(5m):15桁' AS description
                    UNION ALL
                    SELECT '1/250_mesh' AS mesh_type, '1/250メッシュ(4m):15桁' AS description
                    UNION ALL
                    SELECT '1/400_mesh' AS mesh_type, '1/400メッシュ(2.5m):15桁' AS description
                    UNION ALL
                    SELECT '1/500_mesh' AS mesh_type, '1/500メッシュ(2m):15桁' AS description
                    UNION ALL
                    SELECT '1/800_mesh' AS mesh_type, '1/800メッシュ(1.25m):15桁' AS description
                    UNION ALL
                    SELECT '1/1000_mesh' AS mesh_type, '1/1000メッシュ(1m):15桁' AS description),
     mesh_types AS (SELECT UNNEST(ENUM_RANGE(NULL::meshtype)) AS mesh_type)
SELECT t.mesh_type, description
FROM mesh_types AS t
         LEFT JOIN mesh_names AS n
                   ON t.mesh_type::TEXT = n.mesh_type
WHERE t.mesh_type <> 'unknown'
    ;
$$
    LANGUAGE sql;

-- メッシュコードとジオメトリの型を定義する
DROP TYPE IF EXISTS MeshCodeAndGeom CASCADE;
CREATE TYPE MeshCodeAndGeom AS
(
    mesh_type MeshType,
    mesh_code TEXT,
    geom geometry
);

-- ジオメトリ上にあるメッシュを生成する
DROP FUNCTION IF EXISTS get_mesh_polygons_on_geometry(geom geometry, meshtype MeshType);
CREATE OR REPLACE FUNCTION get_mesh_polygons_on_geometry(geom geometry, meshtype MeshType)
    RETURNS SETOF MeshCodeAndGeom
AS
$$
DECLARE
    degree_unit   DOUBLE PRECISION := 3600000;
    w_1st_mesh    DOUBLE PRECISION := 1 * degree_unit;
    h_1st_mesh    DOUBLE PRECISION := degree_unit / 1.5;
    w_2nd_mesh    DOUBLE PRECISION := w_1st_mesh / 8;
    h_2nd_mesh    DOUBLE PRECISION := h_1st_mesh / 8;
    w_3rd_mesh    DOUBLE PRECISION := w_2nd_mesh / 10;
    h_3rd_mesh    DOUBLE PRECISION := h_2nd_mesh / 10;
    w_1_2_mesh    DOUBLE PRECISION := w_3rd_mesh / 2;
    h_1_2_mesh    DOUBLE PRECISION := h_3rd_mesh / 2;
    w_1_4_mesh    DOUBLE PRECISION := w_3rd_mesh / 4;
    h_1_4_mesh    DOUBLE PRECISION := h_3rd_mesh / 4;
    w_1_8_mesh    DOUBLE PRECISION := w_3rd_mesh / 8;
    h_1_8_mesh    DOUBLE PRECISION := h_3rd_mesh / 8;
    w_1_10_mesh   DOUBLE PRECISION := w_3rd_mesh / 10;
    h_1_10_mesh   DOUBLE PRECISION := h_3rd_mesh / 10;
    w_1_20_mesh   DOUBLE PRECISION := w_3rd_mesh / 20;
    h_1_20_mesh   DOUBLE PRECISION := h_3rd_mesh / 20;
    w_1_25_mesh   DOUBLE PRECISION := w_3rd_mesh / 25;
    h_1_25_mesh   DOUBLE PRECISION := h_3rd_mesh / 25;
    w_1_40_mesh   DOUBLE PRECISION := w_3rd_mesh / 40;
    h_1_40_mesh   DOUBLE PRECISION := h_3rd_mesh / 40;
    w_1_50_mesh   DOUBLE PRECISION := w_3rd_mesh / 50;
    h_1_50_mesh   DOUBLE PRECISION := h_3rd_mesh / 50;
    w_1_80_mesh   DOUBLE PRECISION := w_3rd_mesh / 80;
    h_1_80_mesh   DOUBLE PRECISION := h_3rd_mesh / 80;
    w_1_100_mesh  DOUBLE PRECISION := w_3rd_mesh / 100;
    h_1_100_mesh  DOUBLE PRECISION := h_3rd_mesh / 100;
    w_1_200_mesh  DOUBLE PRECISION := w_3rd_mesh / 200;
    h_1_200_mesh  DOUBLE PRECISION := h_3rd_mesh / 200;
    w_1_250_mesh  DOUBLE PRECISION := w_3rd_mesh / 250;
    h_1_250_mesh  DOUBLE PRECISION := h_3rd_mesh / 250;
    w_1_400_mesh  DOUBLE PRECISION := w_3rd_mesh / 400;
    h_1_400_mesh  DOUBLE PRECISION := h_3rd_mesh / 400;
    w_1_500_mesh  DOUBLE PRECISION := w_3rd_mesh / 500;
    h_1_500_mesh  DOUBLE PRECISION := h_3rd_mesh / 500;
    w_1_800_mesh  DOUBLE PRECISION := w_3rd_mesh / 800;
    h_1_800_mesh  DOUBLE PRECISION := h_3rd_mesh / 800;
    w_1_1000_mesh DOUBLE PRECISION := w_3rd_mesh / 1000;
    h_1_1000_mesh DOUBLE PRECISION := h_3rd_mesh / 1000;
    min_x         DOUBLE PRECISION := st_xmin(st_transform(geom, 4612)) * degree_unit;
    max_x         DOUBLE PRECISION := st_xmax(st_transform(geom, 4612)) * degree_unit;
    min_y         DOUBLE PRECISION := st_ymin(st_transform(geom, 4612)) * degree_unit;
    max_y         DOUBLE PRECISION := st_ymax(st_transform(geom, 4612)) * degree_unit;
    mesh_w        DOUBLE PRECISION;
    mesh_h        DOUBLE PRECISION;
    i             INTEGER;
    j             INTEGER;
    i_max         INTEGER;
    j_max         INTEGER;
    code_and_geom MeshCodeAndGeom;
BEGIN
    CASE
        WHEN meshtype = '1st_mesh' THEN mesh_w := w_1st_mesh;
                                        mesh_h := h_1st_mesh;
        WHEN meshtype = '2nd_mesh' THEN mesh_w := w_2nd_mesh;
                                        mesh_h := h_2nd_mesh;
        WHEN meshtype = '3rd_mesh' THEN mesh_w := w_3rd_mesh;
                                        mesh_h := h_3rd_mesh;
        WHEN meshtype = '1/2_mesh' THEN mesh_w := w_1_2_mesh;
                                        mesh_h := h_1_2_mesh;
        WHEN meshtype = '1/4_mesh' THEN mesh_w := w_1_4_mesh;
                                        mesh_h := h_1_4_mesh;
        WHEN meshtype = '1/8_mesh' THEN mesh_w := w_1_8_mesh;
                                        mesh_h := h_1_8_mesh;
        WHEN meshtype = '1/10_mesh_10char' THEN mesh_w := w_1_10_mesh;
                                                mesh_h := h_1_10_mesh;
        WHEN meshtype = '1/20_mesh_11char' THEN mesh_w := w_1_20_mesh;
                                                mesh_h := h_1_20_mesh;
        WHEN meshtype = '1/40_mesh_12char' THEN mesh_w := w_1_40_mesh;
                                                mesh_h := h_1_40_mesh;
        WHEN meshtype = '1/10_mesh' THEN mesh_w := w_1_10_mesh;
                                         mesh_h := h_1_10_mesh;
        WHEN meshtype = '1/20_mesh' THEN mesh_w := w_1_20_mesh;
                                         mesh_h := h_1_20_mesh;
        WHEN meshtype = '1/25_mesh' THEN mesh_w := w_1_25_mesh;
                                         mesh_h := h_1_25_mesh;
        WHEN meshtype = '1/40_mesh' THEN mesh_w := w_1_40_mesh;
                                         mesh_h := h_1_40_mesh;
        WHEN meshtype = '1/50_mesh' THEN mesh_w := w_1_50_mesh;
                                         mesh_h := h_1_50_mesh;
        WHEN meshtype = '1/80_mesh' THEN mesh_w := w_1_80_mesh;
                                         mesh_h := h_1_80_mesh;
        WHEN meshtype = '1/100_mesh' THEN mesh_w := w_1_100_mesh;
                                          mesh_h := h_1_100_mesh;
        WHEN meshtype = '1/200_mesh' THEN mesh_w := w_1_200_mesh;
                                          mesh_h := h_1_200_mesh;
        WHEN meshtype = '1/250_mesh' THEN mesh_w := w_1_250_mesh;
                                          mesh_h := h_1_250_mesh;
        WHEN meshtype = '1/400_mesh' THEN mesh_w := w_1_400_mesh;
                                          mesh_h := h_1_400_mesh;
        WHEN meshtype = '1/500_mesh' THEN mesh_w := w_1_500_mesh;
                                          mesh_h := h_1_500_mesh;
        WHEN meshtype = '1/800_mesh' THEN mesh_w := w_1_800_mesh;
                                          mesh_h := h_1_800_mesh;
        WHEN meshtype = '1/1000_mesh' THEN mesh_w := w_1_1000_mesh;
                                           mesh_h := h_1_1000_mesh;
        END CASE;

    min_x := FLOOR(min_x / mesh_w) * mesh_w - mesh_w / 2;
    max_x := CEIL(max_x / mesh_w) * mesh_w + mesh_w / 2;
    min_y := FLOOR(min_y / mesh_h) * mesh_h - mesh_h / 2;
    max_y := CEIL(max_y / mesh_h) * mesh_h + mesh_h / 2;

    i_max := (max_x - min_x) / mesh_w;
    j_max := (max_y - min_y) / mesh_h;

    FOR i IN 0..i_max
        LOOP
            FOR j IN 0..j_max
                LOOP
                    code_and_geom.mesh_code := get_meshcode_from_point(
                            (min_x + i * mesh_w) / degree_unit,
                            (min_y + j * mesh_h) / degree_unit,
                            meshtype);
                    code_and_geom.geom := get_polygon_from_meshcode(code_and_geom.mesh_code, 4612, meshtype);
                    code_and_geom.mesh_type := meshtype;
                    IF st_intersects(geom, code_and_geom.geom) THEN
                        RETURN NEXT code_and_geom;
                    END IF;
                END LOOP;
        END LOOP;

END;
$$
    LANGUAGE plpgsql;
