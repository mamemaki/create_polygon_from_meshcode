#!/usr/bin/env bash

DB_USER=postgres
DB_HOST=localhost
DB_NAME=get_polygon_from_meshcode
DB_PORT=5432
DB_PASSWORD=postgres
TEMP_DIR=tmp
NEEDS_CLEANUP=true

export PGPASSWORD=$DB_PASSWORD

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# データベース作成
db_count=$(psql -U $DB_USER -h $DB_HOST -l | grep -c $DB_NAME )
if [ "$db_count" -eq 0 ]
then
    createdb -U $DB_USER -h $DB_HOST $DB_NAME
fi

# PostGIS拡張機能を有効化
psql -U $DB_USER -h $DB_HOST -d $DB_NAME -c "create extension if not exists postgis"

# get_polygon_from_meshcode.sqlを実行
psql -U $DB_USER -h $DB_HOST -d $DB_NAME -f $SCRIPT_DIR/../get_polygon_from_meshcode.sql
