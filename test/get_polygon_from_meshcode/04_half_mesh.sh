#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$SCRIPT_DIR" || exit

source ../config.sh

work_dir=$TEMP_DIR/04_half_mesh
table=mesh_half

mkdir -p "$work_dir"

# 1/2メッシュの元ファイルをダウンロード
# e-statから1次メッシュごとにダウンロード
cat 1st_mesh_codes.txt | while read -r code
do
  echo "downloading: $code"
  mesh_url="https://www.e-stat.go.jp/gis/statmap-search/data?dlserveyId=H&code=${code}&coordSys=1&format=shape&downloadType=5"
  wget -O "${work_dir}"/mesh_half_"$code".zip "$mesh_url"
  unzip -d "${work_dir}" "${work_dir}"/mesh_half_"$code".zip
done

 1/2メッシュの元ファイルをデータベースにインポート
first_mesh=$(head -n 1 1st_mesh_codes.txt)
 テーブル作成
shp2pgsql -s 4612 -p -W CP932 "${work_dir}"/MESH0"${first_mesh}".shp $table | psql -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME"
 データ挿入
cat 1st_mesh_codes.txt | while read -r code
do
  shp2pgsql -s 4612 -D -a -W CP932 "${work_dir}"/MESH0"${code}".shp $table | psql -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME"
done

echo "テスト用カラム追加"
psql -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF
-- メッシュコードから生成したメッシュ
ALTER TABLE $table
    ADD IF NOT EXISTS geom_from_mesh geometry(MultiPolygon, 4612);
-- 重なり部分
ALTER TABLE $table
    ADD IF NOT EXISTS overlap geometry(MultiPolygon, 4612);
-- 元メッシュの面積
ALTER TABLE $table
    ADD IF NOT EXISTS area DOUBLE PRECISION;
-- 生成メッシュの面積
ALTER TABLE $table
    ADD IF NOT EXISTS area_from_mesh DOUBLE PRECISION;
-- 重なり部分の面積
ALTER TABLE $table
    ADD IF NOT EXISTS area_overlap DOUBLE PRECISION;
EOF

echo "メッシュコードからメッシュを作成"
psql -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF
UPDATE $table
SET geom_from_mesh = st_multi(get_polygon_from_meshcode(key_code, 4612));
EOF

echo "面積を計算"
psql -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF
UPDATE $table
SET area = st_area(geom);
UPDATE $table
SET area_from_mesh = st_area($table.geom_from_mesh);
UPDATE $table
SET overlap = st_multi(st_intersection(geom, $table.geom_from_mesh));
UPDATE $table
SET area_overlap = st_area(overlap);
EOF

echo "重なっていないものはないか"
psql -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF
SELECT count(*) FROM $table
WHERE st_intersects(geom, geom_from_mesh) = false;
EOF

echo "重なり部分と元メッシュの面積を比較して差が0.01%以上のものがないか"
psql -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF
with overlap_ratio as (
    SELECT gid, area / area_overlap AS ratio
    FROM $table
)
select count(*) from overlap_ratio
where abs(ratio - 1) > 0.0001; -- 0.01%
EOF

if [ "$NEEDS_CLEANUP" = true ]; then
    rm -rf "$work_dir"
    psql -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" -c "DROP TABLE $table"
fi
