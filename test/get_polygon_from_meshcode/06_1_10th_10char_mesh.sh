#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$SCRIPT_DIR" || exit

source ../config.sh

work_dir=$TEMP_DIR/06_1_10th_mesh
table=mesh_1_100th

mkdir -p "$work_dir"

# 3次メッシュの10分の1細分区画のデータをダウンロード
# 国土数値情報 土地利用細分メッシュデータ
mesh_url="https://nlftp.mlit.go.jp/ksj/gml/data/L03-b/L03-b-16/L03-b-16_5132-jgd_GML.zip"
wget -O "${work_dir}"/mesh_1_100th.zip "$mesh_url"
unzip -d "${work_dir}" "${work_dir}"/mesh_1_100th.zip

# 10分の1メッシュのデータをデータベースにインポート
shp2pgsql -s 4612 -I -D -W CP932 "${work_dir}"/L03-b-16_5132.shp $table | psql -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME"

echo "テスト用カラム追加"
psql -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF
-- メッシュコードから生成したメッシュ
ALTER TABLE $table
    ADD IF NOT EXISTS geom_from_mesh geometry(MultiPolygon, 4612);
-- 重なり部分
ALTER TABLE $table
    ADD IF NOT EXISTS overlap geometry(MultiPolygon, 4612);
-- 元メッシュの面積
ALTER TABLE $table
    ADD IF NOT EXISTS area DOUBLE PRECISION;
-- 生成メッシュの面積
ALTER TABLE $table
    ADD IF NOT EXISTS area_from_mesh DOUBLE PRECISION;
-- 重なり部分の面積
ALTER TABLE $table
    ADD IF NOT EXISTS area_overlap DOUBLE PRECISION;
EOF

echo "メッシュコードからメッシュを作成"
psql -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF
UPDATE $table
SET geom_from_mesh = st_multi(get_polygon_from_meshcode("メッシュ", 4612, '1/10_mesh_10char'));
EOF

echo "面積を計算"
psql -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF
UPDATE $table
SET area = st_area(geom);
UPDATE $table
SET area_from_mesh = st_area($table.geom_from_mesh);
UPDATE $table
SET overlap = st_multi(st_intersection(geom, $table.geom_from_mesh));
UPDATE $table
SET area_overlap = st_area(overlap);
EOF

echo "重なっていないものはないか"
psql -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF
SELECT count(*) FROM $table
WHERE st_intersects(geom, geom_from_mesh) = false;
EOF

echo "重なり部分と元メッシュの面積を比較して差が0.01%以上のものがないか"
psql -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF
with overlap_ratio as (
    SELECT gid, area / area_overlap AS ratio
    FROM $table
)
select count(*) from overlap_ratio
where abs(ratio - 1) > 0.0001; -- 0.01%
EOF

if [ "$NEEDS_CLEANUP" = true ]; then
    rm -rf "$work_dir"
    psql -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" -c "DROP TABLE $table"
fi

