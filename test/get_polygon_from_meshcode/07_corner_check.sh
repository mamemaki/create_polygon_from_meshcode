#!/usr/bin/env bash
# テストデータがない細かいメッシュコードのコーナーチェックで対応する

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$SCRIPT_DIR" || exit

source ../config.sh

echo "テスト用関数"
psql -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF
DROP FUNCTION IF EXISTS test_mesh_min_max_on_3rd_mesh(third_mesh TEXT, min_mesh TEXT, max_mesh TEXT, mesh_type MeshType);
CREATE OR REPLACE FUNCTION test_mesh_min_max_on_3rd_mesh(third_mesh TEXT, min_mesh TEXT, max_mesh TEXT,
                                                         mesh_type MeshType DEFAULT NULL)
    RETURNS TABLE (x_min TEXT, x_max TEXT, y_min TEXT, y_max TEXT)
AS
\$\$
WITH mesh_area AS
         (SELECT get_polygon_from_meshcode(third_mesh)                       AS mesh_3rd,
                 get_polygon_from_meshcode(min_mesh, mesh_type := mesh_type) AS mesh_min,
                 get_polygon_from_meshcode(max_mesh, mesh_type := mesh_type) AS mesh_max),
     mesh_diff AS (SELECT st_xmin(mesh_3rd) - st_xmin(mesh_min) AS x_min_diff,
                          st_xmax(mesh_3rd) - st_xmax(mesh_max) AS x_max_diff,
                          st_ymin(mesh_3rd) - st_ymin(mesh_min) AS y_min_diff,
                          st_ymax(mesh_3rd) - st_ymax(mesh_max) AS y_max_diff
                   FROM mesh_area)
SELECT CASE
           WHEN x_min_diff = 0 THEN 'OK: x_min'
           ELSE 'NG: x_min' || x_min_diff
           END AS x_min,
       CASE
           WHEN x_max_diff = 0 THEN 'OK: x_max'
           ELSE 'NG: x_max' || x_max_diff
           END AS x_max,
       CASE
           WHEN y_min_diff = 0 THEN 'OK: y_min'
           ELSE 'NG: y_min' || y_min_diff
           END AS y_min,
       CASE
           WHEN y_max_diff = 0 THEN 'OK: y_max'
           ELSE 'NG: y_max' || y_max_diff
           END AS y_max
FROM mesh_diff;
\$\$ LANGUAGE sql;
EOF

echo "1/2メッシュ/4次メッシュ(500m):9桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '533946211', '533946214', '1/2_mesh');
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '533946211', '533946214');
EOF

echo "1/4メッシュ/5次メッシュ(250m):10桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '5339462111', '5339462144', '1/4_mesh');
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '5339462111', '5339462144');
EOF

echo "1/8メッシュ/6次メッシュ(125m):11桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '53394621111', '53394621444', '1/8_mesh');
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '53394621111', '53394621444');
EOF

echo "3次メッシュの1/10細分区間(100m):10桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '5339462100', '5339462199', '1/10_mesh_10char');
-- これは失敗してOK
SELECT '次は失敗してOK';
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '5339462100', '5339462199');
EOF

echo "3次メッシュの1/20細分区間(50m):11桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '53394621001', '53394621994', '1/20_mesh_11char');
-- これは失敗してOK
SELECT '次は失敗してOK';
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '53394621001', '53394621994');
EOF

echo "3次メッシュの1/40細分区間(25m):12桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '533946210011', '533946219944', '1/40_mesh_12char');
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '533946210011', '533946219944');
EOF

echo "1/10メッシュ(100m):13桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '5339462110000', '5339462110909', '1/10_mesh');
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '5339462110000', '5339462110909');
EOF

echo "1/20メッシュ(50m):13桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '5339462120000', '5339462121919', '1/20_mesh');
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '5339462120000', '5339462121919');
EOF

echo "1/40メッシュ(25m):13桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '5339462130000', '5339462133939', '1/40_mesh');
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '5339462130000', '5339462133939');
EOF

echo "1/50メッシュ(20m):13桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '5339462160000', '5339462164949', '1/50_mesh');
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '5339462160000', '5339462164949');
EOF

echo "1/80メッシュ(12.5m):13桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '5339462140000', '5339462147979', '1/80_mesh');
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '5339462140000', '5339462147979');
EOF

echo "1/100メッシュ(10m):13桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '5339462170000', '5339462179999', '1/100_mesh');
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '5339462170000', '5339462179999');
EOF

echo "1/200メッシュ(5m):15桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '533946212000000', '533946212199199', '1/200_mesh');
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '533946212000000', '533946212199199');
EOF

echo "1/250メッシュ(4m):15桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '533946215000000', '533946215249249', '1/250_mesh');
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '533946215000000', '533946215249249');
EOF

echo "1/400メッシュ(2.5m):15桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '533946213000000', '533946213399399', '1/400_mesh');
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '533946213000000', '533946213399399');
EOF

echo "1/500メッシュ(2m):15桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '533946216000000', '533946216499499', '1/500_mesh');
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '533946216000000', '533946216499499');
EOF

echo "1/800メッシュ(1.25m):15桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '533946214000000', '533946214799799', '1/800_mesh');
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '533946214000000', '533946214799799');
EOF

echo "1/1000メッシュ(1m):15桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '533946217000000', '533946217999999', '1/1000_mesh');
SELECT * FROM test_mesh_min_max_on_3rd_mesh('53394621', '533946217000000', '533946217999999');
EOF

