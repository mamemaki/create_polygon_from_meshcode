#!/usr/bin/env bash
# メッシュをマージしたとき1つポリゴンになるか
# メッシュの座標に桁落ちがあるときには、マージしたときにポリゴンが分割されてしまう

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$SCRIPT_DIR" || exit

source ../config.sh

export boundary_table=tokyo_boundary
work_dir=$TEMP_DIR/01_geom_union_chiyoda
mkdir -p "$work_dir"

# 東京都の行政区域データをダウンロード
# https://nlftp.mlit.go.jp/ksj/gml/data/N03/N03-2024/N03-20240101_13_GML.zip
wget -O "${work_dir}"/N03-20240101_13_GML.zip "https://nlftp.mlit.go.jp/ksj/gml/data/N03/N03-2024/N03-20240101_13_GML.zip"
unzip -d "${work_dir}" "${work_dir}"/N03-20240101_13_GML.zip

psql -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF
DROP TABLE IF EXISTS ${boundary_table};
EOF

# 行政区域データをデータベースにインポート
shp2pgsql -s 4612 -I -D -W UTF-8 "${work_dir}"/N03-20240101_13.shp $boundary_table | psql -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME"

function test_mesh_merge() {
    local mesh_type=$1
    local db_user=$2
    local db_host=$3
    local db_name=$4
    local needs_cleanup=$5

    echo "mesh_type: $mesh_type"
    mesh_type_str=$(echo "$mesh_type" | sed -e 's/\//_/g')
    mesh_table="chiyoda_${mesh_type_str}_meshes"
    merged_table="chiyoda_${mesh_type_str}_merged"

    psql -U "$db_user" -h "$db_host" -d "$db_name" <<EOF
DROP TABLE IF EXISTS ${mesh_table};
-- 千代田区の3次メッシュを作成
CREATE TABLE ${mesh_table} AS
WITH chiyodaku AS (SELECT geom
                   FROM ${boundary_table}
                   WHERE n03_004 = '千代田区')
SELECT *
FROM get_mesh_polygons_on_geometry((SELECT geom FROM chiyodaku), '${mesh_type}');
CREATE INDEX ON ${mesh_table} USING GIST (mesh_geom);

DROP TABLE IF EXISTS ${merged_table};
-- 3次メッシュをマージ
CREATE TABLE ${merged_table} AS
SELECT st_union(mesh_geom) AS geom
FROM ${mesh_table};

-- マージしたポリゴンを確認
SELECT '${mesh_type}'::MeshType, st_numgeometries(geom) = 1 as "マージしたポリゴンの構成要素が一つである"
FROM ${merged_table};
EOF

  if [ "$needs_cleanup" = "true" ]; then
        psql -U "$db_user" -h "$db_host" -d "$db_name" <<EOF
DROP TABLE IF EXISTS ${mesh_table};
DROP TABLE IF EXISTS ${merged_table};
EOF
  fi

}

# 全メッシュタイプでテスト
psql -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" -t -c "SELECT * FROM supported_mesh_types()" | grep " "| sed 's/ //'g | while read -r mesh_type_line
do
    mesh_type=$(echo "$mesh_type_line" | cut -d '|' -f 1)
    mesh_type_desc=$(echo "$mesh_type_line" | cut -d '|' -f 2)
    echo "$mesh_type_desc"
    test_mesh_merge "$mesh_type" "$DB_USER" "$DB_HOST" "$DB_NAME" "$NEEDS_CLEANUP"
done

if [ "$needs_cleanup" = "true" ]; then
        psql -U "$db_user" -h "$db_host" -d "$db_name" <<EOF
DROP TABLE IF EXISTS ${boundary_table};
EOF
fi
