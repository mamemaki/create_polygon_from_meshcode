#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$SCRIPT_DIR" || exit

source ../config.sh

lon=139.7700274114246
lat=35.691254676389114

# テスト用関数
psql -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF
-- ポイントが対応するメッシュの中に含まれるかどうかを判定する関数
DROP FUNCTION IF EXISTS test_point_in_the_mesh(lat DOUBLE PRECISION, lon DOUBLE PRECISION, meshtype MESHTYPE);
CREATE OR REPLACE FUNCTION test_point_in_the_mesh(lat DOUBLE PRECISION, lon DOUBLE PRECISION,
                                                  meshtype MeshType DEFAULT NULL)
    RETURNS TABLE
            (
                code     TEXT,
                meshtype TEXT,
                contains BOOLEAN
            )
AS
\$\$
WITH meshcode AS (SELECT get_meshcode_from_point(lon, lat, meshtype) AS code),
     mesh AS (SELECT code, get_polygon_from_meshcode(code, 4612, meshtype) AS geom
              FROM meshcode)
SELECT code, meshtype, st_contains(geom, st_setsrid(st_makepoint(lon, lat), 4612)) AS contains
FROM mesh;
    ;
\$\$ LANGUAGE sql;
EOF

echo "1次メッシュ:4桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_point_in_the_mesh($lat, $lon, '1st_mesh');
EOF

echo "2次メッシュ:6桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_point_in_the_mesh($lat, $lon, '2nd_mesh');
EOF

echo "3次メッシュ:8桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_point_in_the_mesh($lat, $lon, '3rd_mesh');
EOF

echo "1/2メッシュ/4次メッシュ(500m):9桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_point_in_the_mesh($lat, $lon, '1/2_mesh');
EOF

echo "1/4メッシュ/5次メッシュ(250m):10桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_point_in_the_mesh($lat, $lon, '1/4_mesh');
EOF

echo "1/8メッシュ/6次メッシュ(125m):11桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_point_in_the_mesh($lat, $lon, '1/8_mesh');
EOF

echo "3次メッシュの1/10細分区間(100m):10桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_point_in_the_mesh($lat, $lon, '1/10_mesh_10char');
EOF

echo "3次メッシュの1/20細分区間(50m):11桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_point_in_the_mesh($lat, $lon, '1/20_mesh_11char');
EOF

echo "3次メッシュの1/40細分区間(25m):12桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_point_in_the_mesh($lat, $lon, '1/40_mesh_12char');
EOF

echo "1/10メッシュ(100m):13桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_point_in_the_mesh($lat, $lon, '1/10_mesh');
EOF

echo "1/20メッシュ(50m):13桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_point_in_the_mesh($lat, $lon, '1/20_mesh');
EOF

echo "1/40メッシュ(25m):13桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_point_in_the_mesh($lat, $lon, '1/40_mesh');
EOF

echo "1/50メッシュ(20m):13桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_point_in_the_mesh($lat, $lon, '1/50_mesh');
EOF

echo "1/80メッシュ(12.5m):13桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_point_in_the_mesh($lat, $lon, '1/80_mesh');
EOF

echo "1/100メッシュ(10m):13桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_point_in_the_mesh($lat, $lon, '1/100_mesh');
EOF

echo "1/200メッシュ(5m):15桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_point_in_the_mesh($lat, $lon, '1/200_mesh');
EOF

echo "1/250メッシュ(4m):15桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_point_in_the_mesh($lat, $lon, '1/250_mesh');
EOF

echo "1/400メッシュ(2.5m):15桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_point_in_the_mesh($lat, $lon, '1/400_mesh');
EOF

echo "1/500メッシュ(2m):15桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_point_in_the_mesh($lat, $lon, '1/500_mesh');
EOF

echo "1/800メッシュ(1.25m):15桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_point_in_the_mesh($lat, $lon, '1/800_mesh');
EOF

echo "1/1000メッシュ(1m):15桁"
psql -t -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF | grep " "
SELECT * FROM test_point_in_the_mesh($lat, $lon, '1/1000_mesh');
EOF

# テスト用関数削除
psql -U "$DB_USER" -h "$DB_HOST" -d "$DB_NAME" <<EOF
DROP FUNCTION IF EXISTS test_point_in_the_mesh(lat DOUBLE PRECISION, lon DOUBLE PRECISION, meshtype MESHTYPE);
EOF
